# Maintainer: dodgejcr@gmail.com
# Contributor: Bhushan Shah <bshah@kde.org>
pkgname=libqofono-qt5
pkgver=0.100
pkgrel=1
pkgdesc="A library for accessing the ofono daemon, and a declarative plugin for it. This allows accessing ofono in qtquick and friends."
arch=('aarch64')
url="https://git.merproject.org/mer-core/libqofono"
license=('LGPL')
depends=('qt5-declarative' 'qt5-xmlpatterns')
provides=("libqofono-qt5")
source=("https://git.merproject.org/mer-core/libqofono/-/archive/${pkgver}/libqofono-${pkgver}.tar.gz"
        "0001-add-support-for-reset-contexts.patch"
        "context-preferred.patch"
        "mtk_settings_binding.patch")
md5sums=('1175c0e5f396a8f605092eb98ffb1dd8'
         '18cd69a363041b4b3e72a565ab68b69b'
         '17444b274709abf45a7e871c9f8b6407'
         '19aac206aab5c3e4f115f7e7ba55e541')

prepare() {
    cd libqofono-$pkgver
    mkdir -p build

    # None of these patches break any abi or api, they only add qml functions
    # nor does these patches change the code, only adds code

    # upstreamed patch, adds support for upstream ofono dbus call
    patch -Np1 -i "${srcdir}/0001-add-support-for-reset-contexts.patch"

    # Not upstreamed patches
    patch -Np1 -i "${srcdir}/context-preferred.patch"
    patch -Np1 -i "${srcdir}/mtk_settings_binding.patch"
}

build() {
    cd libqofono-$pkgver/build
    qmake-qt5 PREFIX=/usr ../libqofono.pro
	make
}

package() {
    cd libqofono-$pkgver/build
	make INSTALL_ROOT="$pkgdir/" install
    rm -r "$pkgdir/usr/lib/libqofono-qt5/"
}
